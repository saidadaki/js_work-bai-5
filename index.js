function tinhDiemThi(){
    var diemA = document.getElementById("num__a").value * 1;
    var diemB = document.getElementById("num__b").value * 1;
    var diemC = document.getElementById("num__c").value * 1;
    var diemChuan = document.getElementById("target").value *1;
    var tongDiem = diemA + diemB + diemC;
    if(diemA < 0 || diemB <0 || diemC <0){
        alert("Du lieu khong hop le")
    }
    if(diemA + diemB + diemC == 0 ){
        document.getElementById("result__1").innerHTML = `Bạn bị rớt, xin chia buồn`;
    }
    switch(document.getElementById("area").value){
        case "tv":
            tongDiem = tongDiem;
            break;
        case "A":
            tongDiem = (tongDiem + 2);
            break;
        case "B":
            tongDiem = (tongDiem + 1);
            break;
        case "C":
            tongDiem = (tongDiem +0.5)
    }
    switch (document.getElementById("member").value) {
        case "tv":
            tongDiem = tongDiem;
            break;
        case "1":
            tongDiem = (tongDiem + 2.5);
            break;
        case "2":
            tongDiem = (tongDiem + 1.5);
            break;
        case "3":
            tongDiem = (tongDiem + 1);
            break;
    }
    if(tongDiem >= diemChuan){
        document.getElementById("result__1").innerHTML = `Bạn đã trúng tuyển. Tổng điểm: ${tongDiem}`;
    }else{
        document.getElementById("result__1").innerHTML = `Bạn đã bị rớt. Tổng điểm: ${tongDiem}`;
    }
    document.getElementById("result__1").style.display = "block";
}


function tinhTienDien(){
    var name = document.getElementById("name").value;
    var soDien = document.getElementById("soDien").value *1;
    var giaTienDien = 0;
    const gia50soDau = 500;
    const gia50soSau = 650;
    const gia100soSau = 850;
    const gia150soSau  = 1100;
    const giaConlai = 1300;
    if(soDien<0){
        alert("Du lieu khong hop le");
        document.getElementById("result__2").style.display = "none";
    }else if(soDien ==0){
        giaTienDien = 0;
    }
    else if(soDien <= 50){
        giaTienDien = gia50soDau * soDien;
    }else if(soDien > 50 && soDien <= 100){
        giaTienDien = 50*gia50soDau + ((soDien-50)*gia50soSau);
    }else if(soDien > 100 & soDien <= 200){
        giaTienDien = 50*gia50soDau + 50*gia50soSau + (gia100soSau*(soDien-100));
    }else if(soDien > 200 && soDien <= 350){
        giaTienDien = 50*gia50soDau + 50*gia50soSau + 100*gia100soSau + (gia150soSau*(soDien-200))
    }else{
        giaTienDien = 50*gia50soDau + 50*gia50soSau + 100*gia100soSau + 150*gia150soSau + (giaConlai*(soDien-350))
    }
    document.getElementById("result__2").innerHTML = `Họ và tên: ${name}. Số tiền điện phải đóng: ${giaTienDien.toLocaleString("en-US")}`;
    document.getElementById("result__2").style.display = "inline-block";
}



